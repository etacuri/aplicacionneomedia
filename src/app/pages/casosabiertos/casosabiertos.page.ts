import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RespuestaLogin } from '../../interfaces/interfacesuser';
import { IniciarService } from '../../services/iniciar.service';
import { AlertController, NavController } from '@ionic/angular';
import { RespuestaCaso, Caso } from '../../interfaces/interfacecasos';

@Component({
  selector: 'app-casosabiertos',
  templateUrl: './casosabiertos.page.html',
  styleUrls: ['./casosabiertos.page.scss'],
})
export class CasosabiertosPage implements OnInit {
  usuario: any;
  respuesta: RespuestaCaso;
  casos: Caso[];
    constructor(private route: ActivatedRoute, private servicio: IniciarService, private alertCtrl: AlertController,
                public navCtrl: NavController ) {
    this.usuario = this.route.snapshot.paramMap.get('user');
    console.log(this.route.snapshot.paramMap.get('user'));
  }

  ngOnInit() {
  }

  pedirCasos(){
    this.getCasosAbiertos(this.usuario);
  }
  getCasosAbiertos( ident: string ){
    this.servicio.sendRequestCasos(ident).subscribe(data => {
      if ( data.casos.length > 0 ) {
      console.log(data);
      this.casos = data.casos;
      console.log(this.casos);
    } else {
      this.noExisteAlert();
    }
   }, error => {
    this.noConexionAlert();
  });

  }

  async noConexionAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'ERROR DE CONEXION',
      message: 'No se pudo establecer conexion con el servidor.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async noExisteAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
          subHeader: 'NO TIENE CASOS ASIGNADOS',
          message: 'Al momento no registra casos abiertos',
          buttons: ['OK']
    });
    await alert.present();
  }


  listarLineas(idcaso: string){
    this.navCtrl.navigateForward(`lineacaso/${this.usuario}/${idcaso}`);

  }

  cerrarSession(){
   this.confirmarCierre();
  }
  
  async confirmarCierre() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmacion',
      message: '<strong>Desea cerrar la sesion</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Salir',
          handler: () => {
            this.navCtrl.navigateForward('login');
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
}