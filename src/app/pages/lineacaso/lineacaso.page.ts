import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IniciarService } from 'src/app/services/iniciar.service';
import { RespuestaLinea, CasoLinea } from '../../interfaces/interfacelinea';
import { AlertController, NavController } from '@ionic/angular';


@Component({
  selector: 'app-lineacaso',
  templateUrl: './lineacaso.page.html',
  styleUrls: ['./lineacaso.page.scss'],
})
export class LineacasoPage implements OnInit {
  idtecnico: any;
  idcaso: any;
  respuestalinea: RespuestaLinea;
  caso: CasoLinea[];
  habilitar: boolean;
  coment: string;
    constructor(private route: ActivatedRoute, private servicio: IniciarService, private alertCtrl: AlertController,
                public navCtrl: NavController) {

    this.idtecnico = this.route.snapshot.paramMap.get('idtecnico');
    this.idcaso = this.route.snapshot.paramMap.get('idcaso');

    console.log(this.idcaso);
    console.log(this.idtecnico);
   }

  ngOnInit() {
    this.getCasosAbiertos(this.idtecnico, this.idcaso);
  }


  getCasosAbiertos( idtecnico: string , idcaso: string){
    this.servicio.sendRequestLineas(idtecnico, idcaso).subscribe(data => {
      if ( data.casos.length > 0 ) {
      console.log(data);
      this.caso = data.casos;
      console.log(this.caso);
    } else {
      this.noExisteAlert();
    }
   }, error => {
    this.noConexionAlert();
  });

  }

  async noConexionAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'ERROR DE CONEXION',
      message: 'No se pudo establecer conexion con el servidor.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async noExisteAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
          subHeader: 'NO TIENE CASOS ASIGNADOS',
          message: 'Al momento no registra casos abiertos',
          buttons: ['OK']
    });
    await alert.present();
  }

  putIniciarLinea( idcaso: string ){
    this.servicio.sendPutLinea(idcaso).subscribe(data => {
      if ( data.type === true ) {
      this.navCtrl.navigateForward(`casosabiertos/${this.idtecnico}`);
    } else {
      this.noRegitro();
    }
   }, error => {
    this.noConexionAlert();
  });

  }

  putFinalizarLinea( idcaso: string, comentriorec: string ){
    console.log(comentriorec);
    this.servicio.sendPutFinLinea(idcaso, comentriorec).subscribe(data => {
      if ( data.type === true ) {
      this.navCtrl.navigateForward(`casosabiertos/${this.idtecnico}`);
    } else {
      this.noRegitro();
    }
   }, error => {
    this.noConexionAlert();
  });

  }


  async noRegitro() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
          subHeader: 'ERROR AL REGISTRAR',
          message: 'Se produjo un error al registrar, contacte con el proveedor del servicio',
          buttons: ['OK']
    });
    await alert.present();
  }


}
