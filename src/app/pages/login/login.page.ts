import { Component, OnInit } from '@angular/core';
import { IniciarService } from '../../services/iniciar.service';
import { AlertController, NavController } from '@ionic/angular';
import { RespuestaLogin } from '../../interfaces/interfacesuser';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  u: string;
  p: string;
  userLogin: RespuestaLogin;
  id: string;
  constructor(private loginservice: IniciarService, public alertCtrl: AlertController, public navCtrl: NavController) { }
  ngOnInit() {
  }
  onSubmitLogin() {
    this.loginservice.sendPostRequest(this.u, this.p).subscribe(data => {
        if ( data.verificacion ) {
        console.log('EL USUARIO SI EXISTE ');
        this.userLogin = data;
        this.id = data.idusuario;
        console.log(this.userLogin);
        this.navCtrl.navigateForward(`casosabiertos/${this.id}`);
        } else {
        this.noRegistradoAlert();
      }
     }, error => {
      this.noConexionAlert();
    });
  }

    async noConexionAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'ERROR DE CONEXION',
      message: 'No se pudo establecer conexion con el servidor.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async noRegistradoAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
          subHeader: 'CREDENCIALES INCORRECTAS',
          message: 'El nombre o usuario no son correctos',
          buttons: ['OK']
    });
    await alert.present();
  }

}