import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { RespuestaLogin } from '../interfaces/interfacesuser';
import { RespuestaCaso } from '../interfaces/interfacecasos';
import { RespuestaLinea } from '../interfaces/interfacelinea';
@Injectable({
  providedIn: 'root'
})
export class IniciarService {

  constructor(private http: HttpClient) { }

  getlogin(){}

  sendPostRequest(user: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
      };

    let options = {
        headers: {
          'Content-Type': 'application/json'
        }
      };

    const postData = {
        "user": user,
        "password": password
    };
    return this.http.post<RespuestaLogin>('https://cors-anywhere.herokuapp.com/http://facturas.atrums.com/wsAtrumsEcNeomedia/wsatrumsrestful/ingresoapp', postData,
    httpOptions);
}

  sendRequestCasos(id: string) {
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
      })
    };
  const postData = {
      "idtecnico": id
      }
  return this.http.post<RespuestaCaso>('https://cors-anywhere.herokuapp.com/http://facturas.atrums.com/wsAtrumsEcNeomedia/wsatrumsrestful/casosabierto',
                                        postData, httpOptions);
}
sendRequestLineas(idtecnico: string, idcaso: string) {
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      })
    };
  const postData = {
      "idtecnico": idtecnico,
      "idcaso": idcaso
      }
  return this.http.post<RespuestaLinea>('https://cors-anywhere.herokuapp.com/http://facturas.atrums.com/wsAtrumsEcNeomedia/wsatrumsrestful/lineascaso', 
                                        postData, httpOptions);
}
sendPutLinea(idcaso: string) {
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      })
    };
  const postData = {
      "idlinea": idcaso
      }

  return this.http.put<RespuestaPut>('https://cors-anywhere.herokuapp.com/http://facturas.atrums.com/wsAtrumsEcNeomedia/wsatrumsrestful/registrarinicio', 
                                      postData, httpOptions);
}
sendPutFinLinea(idcaso: string, comentario: string) {
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      })
    };
  const postData = {
      "idlinea": idcaso,
      "comentario":comentario
      }
  return this.http.put<RespuestaPut>('https://cors-anywhere.herokuapp.com/http://facturas.atrums.com/wsAtrumsEcNeomedia/wsatrumsrestful/registrarfin', 
                                      postData, httpOptions);
}
}