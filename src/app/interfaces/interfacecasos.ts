export interface RespuestaCaso {
  usuario: string;
  casos: Caso[];
}
export interface Caso {
  idcaso: string;
  nombrecaso: string;
  nombreagencia: string;
  nombrecanal: string;
  numcaso: string;
  descrpcion: string;
  nombrecliente: string;
}