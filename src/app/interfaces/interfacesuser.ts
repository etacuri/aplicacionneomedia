export interface RespuestaLogin {
  usuario: string;
  verificacion: boolean;
  idusuario: string;
}