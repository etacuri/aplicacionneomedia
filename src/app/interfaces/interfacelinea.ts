export interface RespuestaLinea {
  casos: CasoLinea[];
}
export interface CasoLinea {
  nombrecaso: string;
  idlinea: string;
  nombretecnico: string;
  estadolinea: boolean;
  comentario: string;
}